# Configuration steps for Kubernetes cluster with 3 Cassandra nodes
## Installing kubectl and checking that it works
```
carolina@ubuntu:~$ curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
% Total    % Received % Xferd  Average Speed   Time    Time     Time  Current  Dload  Upload   Total   Spent    Left  Speed
100 41.4M  100 41.4M    0     0  10.4M      0  0:00:03  0:00:03 --:--:-- 10.4M
carolina@ubuntu:~$ chmod +x ./kubectl
carolina@ubuntu:~$ sudo mv ./kubectl /usr/local/bin/kubectl
carolina@ubuntu:~$ kubectl version --client
Client Version: version.Info{Major:"1", Minor:"17", GitVersion:"v1.17.2", GitCommit:"59603c6e503c87169aea6106f57b9f242f64df89", GitTreeState:"clean", BuildDate:"2020-01-18T23:30:10Z", GoVersion:"go1.13.5", Compiler:"gc", Platform:"linux/amd64"}
```
## Installing minikube and checking that it works
```
carolina@ubuntu:~$ curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \>    && chmod +x minikube
% Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                             Dload  Upload   Total   Spent    Left  Speed
100 46.7M  100 46.7M    0     0  10.2M      0  0:00:04  0:00:04 --:--:-- 11.3M
carolina@ubuntu:~$ sudo mkdir -p /usr/local/bin/
carolina@ubuntu:~$ sudo install minikube /usr/local/bin/
carolina@ubuntu:~$ minikube start --vm-driver=none
😄  minikube v1.7.2 on Ubuntu 18.04
✨  Using the none driver based on user configuration
💡  Requested cpu count 1 is less than the minimum allowed of 2
```

## Troubleshooting the cpu count
- Add more CPUs to vmware VM and turn on virtualization too.
- Try the virtualbox driver (kvm seems too complicated for simple examples, manual setup..)
```
  carolina@ubuntu:~$ minikube delete
  🙄  "minikube" profile does not exist, trying anyways.
  🔥  Trying to delete invalid profile minikube
  💣  open /home/carolina/.minikube/profiles: permission denied
  carolina@ubuntu:~$ sudo mv .minikube/ .minikube-broken
```
## Using the virtualbox driver
```
    carolina@ubuntu:~$ minikube start --vm-driver=virtualbox
    carolina@ubuntu:~$ minikube status
    host: Running
    kubelet: Running
    apiserver: Running
    kubeconfig: Configured
```

## Testing with the Kubernetes dashboard
```
carolina@ubuntu:~$  kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-rc5/aio/deploy/recommended.yaml
namespace/kubernetes-dashboard created
serviceaccount/kubernetes-dashboard created
service/kubernetes-dashboard created
secret/kubernetes-dashboard-certs created
secret/kubernetes-dashboard-csrf created
secret/kubernetes-dashboard-key-holder created
configmap/kubernetes-dashboard-settings created
role.rbac.authorization.k8s.io/kubernetes-dashboard created
clusterrole.rbac.authorization.k8s.io/kubernetes-dashboard created
rolebinding.rbac.authorization.k8s.io/kubernetes-dashboard created
clusterrolebinding.rbac.authorization.k8s.io/kubernetes-dashboard created
deployment.apps/kubernetes-dashboard created
service/dashboard-metrics-scraper created
deployment.apps/dashboard-metrics-scraper created
```

## Create adminuser (safe for local experiment setup) to log in with token
```
carolina@ubuntu:~/.minikube/config$ touch dashboard-adminuser.yaml
carolina@ubuntu:~/.minikube/config$ vim dashboard-adminuser.yaml 
carolina@ubuntu:~/.minikube/config$ cat dashboard-adminuser.yaml 
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kubernetes-dashboard

apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kubernetes-dashboard
carolina@ubuntu:~/.minikube/config$ kubectl apply -f dashboard-adminuser.yaml
clusterrolebinding.rbac.authorization.k8s.io/admin-user created
```

## Get the token from the output and log in
```
carolina@ubuntu:~/.minikube/config$ kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')
``` 
- Need to modify URL to actually land on the dashboard webpage to log in.
- Up to now it took about 1.5h.

## Installing cassandra
- Following this example: `https://kubernetes.io/docs/tutorials/stateful-application/cassandra/`
```
carolina@ubuntu:~/.minikube/config$ wget https://kubernetes.io/examples/application/cassandra/cassandra-statefulset.yaml
carolina@ubuntu:~/.minikube/config$ wget https://kubernetes.io/examples/application/cassandra/cassandra-service.yaml
```

- Need to restart VM and add more cores to be able to do this: minikube start --memory 5120 --cpus=4
```
carolina@ubuntu:~$ minikube start --vm-driver=virtualbox --memory 5120 --cpus=4
```

- Start the dashboard again
```
carolina@ubuntu:~$ kubectl apply -f ~/.minikube/config/dashboard-adminuser.yaml
serviceaccount/admin-user created
carolina@ubuntu:~$ kubectl apply -f ~/.minikube/config/dashboard-clusterrolebinding.yaml 
clusterrolebinding.rbac.authorization.k8s.io/admin-user created
carolina@ubuntu:~$ kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')
```

## Start Cassandra service based on basic configuration
```
carolina@ubuntu:~/.minikube/config$ ls
cassandra-service.yaml  cassandra-statefulset.yaml  config.json  dashboard-adminuser.yaml  dashboard-clusterrolebinding.yaml
carolina@ubuntu:~/.minikube/config$ kubectl apply -f cassandra-service.yaml
service/cassandra created
carolina@ubuntu:~/.minikube/config$ kubectl get svc cassandra
NAME        TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)    AGE
cassandra   ClusterIP   None         <none>        9042/TCP   11s
carolina@ubuntu:~/.minikube/config$ kubectl apply -f cassandra-statefulset.yaml 
statefulset.apps/cassandra created
storageclass.storage.k8s.io/fast created
carolina@ubuntu:~/.minikube/config$ kubectl get statefulset cassandra
NAME        READY   AGE
cassandra   0/3     16s
carolina@ubuntu:~/.minikube/config$ kubectl get statefulset cassandra
NAME        READY   AGE
cassandra   3/3     4m38s
carolina@ubuntu:~/.minikube/config$ kubectl get pods -l="app=cassandra"
NAME          READY   STATUS    RESTARTS   AGE
cassandra-0   1/1     Running   0          4m42s
cassandra-1   1/1     Running   0          3m37s
cassandra-2   1/1     Running   0          2m1s
carolina@ubuntu:~/.minikube/config$ kubectl exec -it cassandra-0 -- nodetool status
Datacenter: DC1-K8Demo
======================
Status=Up/Down
|/ State=Normal/Leaving/Joining/Moving
--  Address     Load       Tokens       Owns (effective)  Host ID                               Rack
UN  172.17.0.7  103.8 KiB  32           58.8%             84a95c9f-c6ba-4098-96c6-1dd416319548  Rack1-K8Demo
UN  172.17.0.6  104.54 KiB  32           75.6%             29ce3c5e-8b36-4105-ac86-fd4c76c9159a  Rack1-K8Demo
UN  172.17.0.8  108.89 KiB  32           65.6%             fc44e1ad-258b-4211-a28f-e26f6f5c8fe6  Rack1-K8Demo
```

## Create keyspace and some data in cassandra
- First port forwarding to local machine:
```
carolina@ubuntu:~/.minikube/config$ kubectl port-forward cassandra-0 9042:9042
Forwarding from 127.0.0.1:9042 -> 9042
Forwarding from [::1]:9042 -> 9042
```
- Then try to connect, first from inside one of the Cassandra instances:
```
carolina@ubuntu:~/.minikube/config$ kubectl exec -it cassandra-0 -- /bin/bash
```
- The instance needs python to run cqlsh.. (Anyway it makes more sense to have a client outside.)

## Installing a cqlsh client
- Install python3 and pip3 install cqlsh using `apt install`... but this gives no CLI client.
- Install a client from here: `https://docs.datastax.com/en/install/6.7/install/installCqlsh.html`
- Try to connect... get error that protocol version 5 is not supported
- Check help and manually give an integer ... be lucky:
```
carolina@ubuntu:~/Documents/cqlsh-5.1.17/bin$ ./cqlsh --protocol-version=1
Connection error: ('Unable to connect to any servers', {'127.0.0.1': ProtocolError('This version of the driver does not support protocol version 1',)})
carolina@ubuntu:~/Documents/cqlsh-5.1.17/bin$ ./cqlsh --protocol-version=2
Connection error: ('Unable to connect to any servers', {'127.0.0.1': ProtocolError('This version of the driver does not support protocol version 2',)})
carolina@ubuntu:~/Documents/cqlsh-5.1.17/bin$ ./cqlsh --protocol-version=3
Connected to K8Demo at 127.0.0.1:9042.
[cqlsh 5.0.1 | Cassandra 3.11.2 | DSE  | CQL spec 3.4.4 | Native protocol v4]
Use HELP for help.
cqlsh> 
.....
```
- Insert some data and verify it is stored:
```
cqlsh> create keyspace animalkeyspace with replication = {'class':'SimpleStrategy', 'replication_factor':1}
   ... ;
cqlsh> use animalkeyspace ;
cqlsh:animalkeyspace> create table monkey (identifier uuid, species text, nickname text, population int, primary key ((identifier),species));
cqlsh:animalkeyspace> insert into monkey (identifier, species, nickname, population) values (12345, 'Capuchin monkey', cutiepie, 1000);
cqlsh:animalkeyspace> select * from monkey
                  ... ;

 identifier                           | species         | nickname | population
--------------------------------------+-----------------+----------+------------
 5132b130-ae79-11e4-ab27-0800200c9a66 | Capuchin monkey | cutiepie |       1000

(1 rows)
cqlsh:animalkeyspace> insert into monkey (identifier, species, nickname, population) values (5132b130-ae79-11e4-ab27-0800200c9a67, 'Gorilla', 'GoGo', 10);
cqlsh:animalkeyspace> select * from monkey ;

 identifier                           | species         | nickname | population
--------------------------------------+-----------------+----------+------------
 5132b130-ae79-11e4-ab27-0800200c9a66 | Capuchin monkey | cutiepie |       1000
 5132b130-ae79-11e4-ab27-0800200c9a67 |         Gorilla |     GoGo |         10
```

## Create a persistent volume for the backup and connect it to the cluster
- Duplicate the volume config to create a backup volume. According to docs the volume can't be shared (maybe there is a way, but did not find it), so three separate volumes should be created. This also distributes the backup.

- First try patching the statefulset so that it can be changed with an update:
- Following the documentation for patching here: `https://kubernetes.io/docs/tasks/run-application/update-api-object-kubectl-patch/`
```
carolina@ubuntu:~/.minikube/config$ kubectl patch statefulset cassandra -p '{"spec":{"updateStrategy":{"type":"RollingUpdate"}}}'
statefulset.apps/cassandra patched (no change)
```
- Attempt to create a patch:
```
spec:
  template:
    spec:
      containers:
        volumeMounts:
        - name: cassandra-backup
          mountPath: /cassandra_backup
  # These are converted to volume claims by the controller
  # and mounted at the paths mentioned above.
  # do not use these in production until ssd GCEPersistentDisk or other ssd pd
  volumeClaimTemplates:
  - metadata:
      name: cassandra-backup
    spec:
      accessModes: [ "ReadWriteOnce" ]
      storageClassName: fast
      resources:
        requests:
          storage: 1Gi
```
- Attempt to patch:
```
carolina@ubuntu:~/.minikube/config$ kubectl patch statefulset cassandra --patch "$(cat cassandra-backup-patch.yaml)"
The request is invalid: patch: Invalid value: "map[spec:map[template:map[spec:map[containers:map[volumeMounts:[map[mountPath:/cassandra_backup name:cassandra-backup]]]]] volumeClaimTemplates:[map[metadata:map[name:cassandra-backup] spec:map[accessModes:[ReadWriteOnce] resources:map[requests:map[storage:1Gi]] storageClassName:fast]]]]]": cannot restore slice from map
```
- If the intended end result, i.e. 3 separate volumes for backup could be created, then a cronjob or a manually triggered script should be added that copies the files to back up using `nodetools`.
- In order to restore the backup `sstableloader` should be used.
## Other ideas:
- Perhaps even `kubectl cp` could be used for copying the required files in and out of the containers: `https://medium.com/@tharangarajapaksha/kubernetes-volumes-79ff58ec32e8`




